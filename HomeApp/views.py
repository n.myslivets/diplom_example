from django.shortcuts import render


def main_home(request):
    return render(request, template_name='main_home.html')
